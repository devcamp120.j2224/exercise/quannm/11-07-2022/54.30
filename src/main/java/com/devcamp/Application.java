package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

		// Khai báo đối tượng class CRandomNumber
		CRandomNumber randomNumber = new CRandomNumber();
		// In ra số ngẫu nhiên kiểu double từ 1 đến 100
		System.out.println(randomNumber.randomDoubleNumber());
		// In ra số ngẫu nhiên kiểu int từ 1 đến 10
		System.out.println(randomNumber.randomIntNumber());
	}

}
